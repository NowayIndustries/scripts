#Name: planet_osfast_start_server
#Date: 2014-03-04
#Author: Sjors
#Purpose: Simple script which starts the server in a named screen.

serverlocation="/home/osfast/minecraft/"

cd $serverlocation
startcommand="java -Xmx3096M -jar $(ls -t minecraft_server.*.jar | head -1) nogui"

screen -R minecraft_server

#If you want a specific jar run, alter the last line, and uncomment it whilst commenting the one above it.
screen -R minecraft_server -X stuff "$startcommand $(printf '\r')"
#screen -R minecraft_server -X stuff "java -Xmx3096M -jar minecraft_server.1.7.5.jar $(printf '\r')"
