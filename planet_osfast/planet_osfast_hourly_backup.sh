#!/bin/bash
#Name: planet_osfast_hourly_backup.sh
#Date: 2014-03-02
#Author: Sjors Nowee
#Purpose: To backup a folder to a location, to be executed every hour giving the file a timestamp based filename.
#Credits: xmodulo.com - Bash timestamping example

#--Settings--#
#Don't forget to remove the first and last '/'
backuptarget="home/osfast/Minecraft"	#Location to backup
backuplocation="home/osfast"			#Where to place backups
filenameprefix="hourly" 				#Before timestamp, start of name '.' added after automatically.
preserveDays=1							#How many days of backups should remain? (1 means only today's backups are kept)
cleanup=true							#Should I cleanup previous backups? (see preservedays) REQUIRES REPORT FUNCTIONALITY
report=true								#Should I write to a logfile? (log filename is 'backuplog_date.log'
debug=true								#Don't actually remove the backup files, just log (testing for removal method functionality)
notify=true								#Broadcast to players about backups.
#--sgnitteS--#

timestamp=$(date +"%Y-%m-%d:%H")
backupname="/$backuplocation/$filenameprefix.$timestamp.tar.gz"

if($notify) then
	screen -R minecraft_server -X stuff "say Starting backup, temporarily disabling worldsaving. $(printf '\r')"
fi
screen -R minecraft_server -X stuff "save-off $(printf '\r')"
tar -cpzf $backupname -C / $backuptarget > /dev/null 2>/dev/null
screen -R minecraft_server -X stuff "save-on $(printf '\r')"
if($notify) then
	screen -R minecraft_server -X stuff "say Finished backup, worldsaving has been enabled. $(printf '\r')"
fi

if ($report) then
	logdatestamp=$(date +"%Y-%m-%d")
	logtimestamp=$(date +"%T")
	echo "[$logtimestamp] '$backuptarget/' has been backed up as '$backupname'." >> /$backuplocation/backuplog_$logdatestamp.log
	
	if($cleanup) then
		if($debug) then
			echo "[$logtimestamp] (Script is in debug mode!) DID NOT REMOVE '$( find /$backuplocation/$filenameprefix.$(date +"%Y-%m-%d" -d "$preserveDays day ago"):$(date +%H).tar.gz )'" >> /$backuplocation/backuplog_$logdatestamp.log
		elif(! $debug) then
			echo "[$logtimestamp] $( find /$backuplocation/$filenameprefix.$(date +"%Y-%m-%d" -d "$preserveDays day ago"):$(date +%H).tar.gz -exec rm -v {} \;)." >> /$backuplocation/backuplog_$logdatestamp.log
		else
			echo "Something went horribly wrong :("
		fi
	fi
fi
